# Web Auth Demo

Complete the follow project based on the instructions in the comments.  It used nodemon to serve on port 3000, unless another port has been specified in the Node Environment variables.

### .env File
Create a .env file with your database credentials. You must specify the following items:
DB_DATABASE=
DB_USER=
DB_PASS=
DB_HOST=
DB_DIALECT=mysql
HASH_SECRET=

### Node Express
- Auth Middleware
- /login POST request

### Front End (www)
- Fetch request in index.js
- Redirect to dashboard

## Install
To install required packages you must run
```
npm install
```

## Run application
To run the application you must run:
```
npm start
```